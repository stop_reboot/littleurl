var path = require('path');

module.exports = {
    "db": {
        "file_path": "data/littleurl.db",
        "queries": {
            "db_exists": "SELECT name FROM sqlite_master WHERE type='table' AND name='urls'",
            "create_urls_table": "CREATE TABLE IF NOT EXISTS 'urls'(`id`	TEXT PRIMARY KEY NOT NULL UNIQUE,  `long_url` TEXT UNIQUE NOT NULL);",
            "insert_data": "INSERT INTO 'urls' (id, long_url) VALUES(?, ?)",
            "select_by_url": "SELECT * FROM urls WHERE long_url = ?",
            "select_by_id": "SELECT * FROM urls WHERE id = ?",
            "select_all_ordered": "SELECT * FROM urls ORDER BY long_url"
        },
        "bulk_urls": "data/bulk_links.list"
    },
    "logger": {
        "api": "logs/api.log",
        "web": "logs/web/log",
        "exception": "logs/exceptions.log"
    },
    "server": {
        "port": "3000",
        "base_url": "http://localhost"
    },
    "app":{
        "alias_length": "10"
    }

};
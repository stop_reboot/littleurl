var chai = require('chai');
var expect = chai.expect;
chai.should();
var urlAlias = require('../lib/urlAlias');

describe('urlAlias', function() {

    before(function (done) {
        done();
    });

    after(function (done) {
        done();
    });

    describe('#getDataHash', function() {
        it('Should return the same value for a given string', function() {
            var test_data = "TEST";
            var sample_result = "AzvZSxFo1+Tw1kTDyV41vw==";
            var value = urlAlias.getDataHash(test_data);
            value.should.be.a('string');
            value.should.equal(sample_result);
            value.should.have.length(sample_result.length);

        });

        it('Should return the same value for an empty string', function() {
            var test_data = "";
            var sample_result = "1B2M2Y8AsgTpgAmY7PhCfg==";
            var value = urlAlias.getDataHash(test_data);
            value.should.be.a('string');
            value.should.equal(sample_result);
            value.should.have.length(sample_result.length);

        });

        it('Should throw a TypeError when given no arguments', function() {
           expect(function(){urlAlias.getDataHash()}).to.Throw(TypeError && "Not a string or buffer");

        });
    });

    describe('#getRandomIntInclusive', function() {
        it('Should return all values in the range, and none outside of the range', function() {
            var i;
            var arr = [];
            var min = 1;
            var max = 5;

            for(i = 0; i < 100; i++)
            {
                arr.push(urlAlias.getRandomIntInclusive(min, max));
            }

            arr.should.include(1);
            arr.should.include(2);
            arr.should.include(3);
            arr.should.include(4);
            arr.should.include(5);
            arr.should.not.include(0);
            arr.should.not.include(6);
            arr.should.not.include(-1);
            arr.should.not.include(0.5);
        })
    });

    describe('#getRandomString', function() {
        it('Should have numeral, lowercase, and uppercase', function() {
            var value = urlAlias.getRandomString(1000);
            value.should.be.a('string');
            value.should.have.length(1000);
            value.should.contain('0');
            value.should.contain('t');
            value.should.contain('M');
        });

        it('Should return a string with the requested length', function() {
            var i;
            for(i = 0;i<10;i++)
            {
                urlAlias.getRandomString(7).should.have.length(7);
            }
        });

        it('Should not have any duplicates within a small sample', function() {
            var i;
            var arr= [];
            for(i = 0;i<20;i++)
            {
                var val = urlAlias.getRandomString(7);
                arr.should.not.include(val);
                arr.push(val);
            }
        });

        it('Should give an empty string when length arg is 0', function() {
            var value = urlAlias.getRandomString(0);
            value.should.be.a('string');
            value.should.have.length(0);
            value.should.be.empty;
        });
    });

});
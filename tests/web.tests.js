var chai = require('chai');

chai.should();
var request = require('supertest');
var urlAlias = require('../lib/urlAlias');
var assert = require('assert');


describe('server', function() {
    // Beforehand, start the server
    before(function (done) {
        server = require('../bin/www');
        console.log('Starting the server');
        done();
    });

    // Afterwards, stop the server and empty the database
    after(function (done) {
        console.log('Stopping the server');
        server.close();
        setTimeout(done, 1000);
        done();
    });

    describe('All', function () {

        it('Responds with 200',function(done){
            request(server)
                .get('/')
                .expect(200, done);
        });

        it('Content type text/html when application/json sent',function(done){
            request(server)
                .get('/')
                .set('Accept', 'application/json')
                .expect('Content-Type', 'text/html; charset=utf-8')
                .expect(200, done);
        });

        it('Responds with 404 for alias that has not been created',function(done){
            request(server)
                .get('/show/this-should-not-exist')
                .set('Accept', 'application/json')
                .expect('Content-Type', 'text/html; charset=utf-8')
                .expect(404, done);
        });

        it("posts a new url succeeds with correct response", function(done){

            var target_url = "http://www.test" + urlAlias.getRandomIntInclusive(0, 1000) + ".com";
            var data_obj = {
                title: "Index",
                submitted_url: target_url,
                base_url: "http://localhost",
                generated_id: "test01"
            };
            request("http://localhost")
                .post("/")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Index"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {
                    assert.equal(result.body.base_url, "http://localhost/show");
                    assert.equal(result.body.submitted_url, target_url);
                    assert.equal(result.body.generated_id.length, 10);
                    done();
                });
        });

        it("posts with duplicate returns duplicate id", function(done){

            var target_url = "http://www.sampletest.com";
            var expected_id;
            var data_obj = {
                title: "Index",
                submitted_url: target_url,
                base_url: "http://localhost"
            };

            request("http://localhost")
                .post("/")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Index"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {
                    assert.equal(result.body.base_url, "http://localhost/show");
                    assert.equal(result.body.submitted_url, target_url);
                    assert.equal(result.body.generated_id.length, 10);
                    expected_id = result.body.generated_id;

                });

            request("http://localhost")
                .post("/")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Index"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {
                    assert.equal(result.body.base_url, "http://localhost/show");
                    assert.equal(result.body.submitted_url, target_url);
                    assert.equal(result.body.generated_id.length, 10);
                    assert.equal(result.body.generated_id, expected_id);
                    done();
                });
        });

        it("request to saved id redirects to correct page", function(done){

            var target_url = "http://www.google.com";
            var expected_id;
            var data_obj = {
                title: "Index",
                submitted_url: target_url,
                base_url: "http://localhost"
            };
            request("http://localhost")
                .post("/")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Index"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {
                    assert.equal(result.body.base_url, "http://localhost/show");
                    assert.equal(result.body.submitted_url, target_url);
                    assert.equal(result.body.generated_id.length, 10);
                    expected_id = result.body.generated_id;
                    request("http://localhost")
                        .get("/show/"+expected_id)
                        .send(data_obj)
                        .expect(301)
                        .expect('Found. Redirecting to http://www.google.com')
                        .expect(/"title":"Index"/)
                        .expect('Location', 'http://www.google.com')
                        .expect('Content-Type', "application/json; charset=utf-8")
                        .end(function(err, result) {

                            assert.equal(result.headers.location, target_url);
                            done();
                        });
                });

        });

        it("request to show listing shows at least one known item", function(done){

            var target_url = "https://en.wikipedia.org/wiki/Main_Page";
            var expected_id;
            var data_obj = {
                title: "Index",
                submitted_url: target_url,
                base_url: "http://localhost"
            };

            request("http://localhost")
                .post("/")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Index"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {
                    assert.equal(result.body.base_url, "http://localhost/show");
                    assert.equal(result.body.submitted_url, target_url);
                    assert.equal(result.body.generated_id.length, 10);
                    expected_id = result.body.generated_id;
                });

            request("http://localhost")
                .get("/show")
                .send(data_obj)
                .expect(200)
                .expect(/"title":"Show"/)
                .expect('Content-Type', "application/json; charset=utf-8")
                .end(function(err, result) {

                    assert.equal(result.text.indexOf(target_url) > -1, true);
                    done();
                });

        });

    });

});
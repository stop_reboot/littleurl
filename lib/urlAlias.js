"use strict";

var config = require("../config.js");
var path = require("path");
var crypto = require("crypto");

config.collisions=0;

function getDataHash(data) {
    return crypto.createHash('md5').update(data).digest('base64');
}

function getRandomIntInclusive(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomString(len) {
    var i;
    var randomString = '';
    var ALLOWED_CHARACTERS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    var SET_SIZE = ALLOWED_CHARACTERS.length;
    var MIN = 0;
    var MAX = SET_SIZE - 1;

    for (i = 0; i < len; i++) {
        randomString += ALLOWED_CHARACTERS[getRandomIntInclusive(MIN, MAX)];
    }
    return randomString;
}

module.exports.getDataHash = getDataHash;
module.exports.getRandomIntInclusive = getRandomIntInclusive;
module.exports.getRandomString = getRandomString;
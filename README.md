# Description
App that creates an alias for a URL, and redirects the browser to the URL when the alias is clicked.

# Testing
Run ``grunt test``. Tests run against 'http://localhost', but the site is configured to use 'http://localhost:3000'.
This means that you should have nginx running.
Use ``pm2`` to start the site, and monitor it.

TODO: Sanitize URL before storing in database, and before displaying on page.
TODO: Throttle request
TODO: Track usage data
TODO: Prevent links to self
TODO: Blacklist potentially dangerous sites
TODO: Strip trailing / from URL
TODO: Limit maximum length of URL
TODO: Limit protocols supported
TODO: FEATURE: Add delete option
TODO: FEATURE: Better recovery and prediction of ID collision
TODO: FEATURE: Pagination for list view
TODO: Extract methods for db access into library
TODO: Linting. Not urgent but could help.
TODO: FEATURE: Logging with morgan
TODO: FEATURE: Additional fields tracked for analytics
TODO: FEATURE: Minify to improve performance
TODO: FEATURE: Support for mobile devices, other browsers.
TODO: Generate REST api

# Commands
To run this, you should have nginx and pm2 installed, configured, and running

sudo rm /etc/nginx/sites-enabled/default
sudo cp shorturl_nginx_config /etc/nginx/sites-available/short_url

sudo ln -s /etc/nginx/sites-available/short_url /etc/nginx/sites-enabled

sudo service nginx reload
sudo pm2 start bin/www --name "shorturl" -i max
pm2 startup ubuntu



service --status-all
sudo service nginx stop
sudo pm2 kill


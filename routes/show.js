"use strict";
var express, router, urlHash, database, config, sqlite3;

express = require('express');
router = express.Router();
urlHash = require('../lib/urlAlias');
sqlite3 = require('sqlite3').verbose();
config = require('../config');
database = new sqlite3.Database(config.db.file_path);


function getAllFromDB(req, res, next) {
    console.log('Getting all entries from database');
    var base_url = req.app.settings.base_url + '/show/';
    database.all(config.db.queries.select_all_ordered, function(err, rows) {
        /* istanbul ignore if */
        if(err !== null) {next(err);}
        else {
            res.render('show.jade', {title: "Show", pid: process.pid,  base_url: base_url, rows: rows });
        }
    });
}

function getFromDBbyID(req, res, next){
    var id = req.params.id;
    console.log ("Getting ID " + id);
    database.get(config.db.queries.select_by_id, id, function(err, row) {
        /* istanbul ignore if */
        if(err !== null) {next(err);}
        else if(!row) {
            console.log("Could not find the url.");
           next(err);
        }
        else{

            //console.log("Found in DB: " + row);
            console.log("Redirecting to: " + row.long_url);
            res.redirect(row.long_url);
        }
    });
}

router.get('/', getAllFromDB );
router.get('/:id', getFromDBbyID );

module.exports = router;
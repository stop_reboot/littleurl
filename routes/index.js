"use strict";

var express, router, urlHash, sqlite3, config, database;

express = require('express');
router = express.Router();
urlHash = require('../lib/urlAlias');
sqlite3 = require('sqlite3').verbose();
config = require('../config');

database = new sqlite3.Database(config.db.file_path);

function getFromDBbyURL(req, res, next){
    var submitted_url = req.body.submitted_url;
    var base_url = req.app.settings.base_url + '/show';

    database.get(config.db.queries.select_by_url, submitted_url, function(err, row) {
        /* istanbul ignore if */
        if(err !== null) {next(err);}
        else {
            console.log(row);
            var json_object = {
                title: 'Index',
                submitted_url: submitted_url,
                base_url: base_url,
                generated_id: row.id
            };
            res.send(json_object);
        }
    });
}

function addToDB(req,res,next) {
    var submitted_url, generated_url_id;

    console.log("Received POST on /");
    var id = req.params.id;

    submitted_url = req.body.submitted_url;
    console.log("Submitted URL is: " + submitted_url);

    var sqlInsert = config.db.queries.insert_data; //"INSERT INTO 'urls' (id, long_url) VALUES(?, ?)";

    generated_url_id = urlHash.getRandomString(config.app.alias_length);

    database.run(sqlInsert, generated_url_id, submitted_url, function(err) {
        if(err !== null) {}
        else {console.log("Looking in database for existing url");}
        getFromDBbyURL(req,res,next);
    });
}

router.get('/', function (req, res, next) {
    console.log("Received GET on /");
    res.render('index', {title: 'Index', pid: process.pid});
});

router.post('/', addToDB );

module.exports = router;

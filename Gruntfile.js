module.exports = function (grunt) {
'use strict';
    // Configure the rules, tools, and reports
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: { 
            files: ['gruntfile.js', 'lib/**/*.js', 'tests/**/*.js'],
            all: [ 'tests/*.js', 'index.js' ] ,
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    node: true,
                    nonstandard: true
                }
            }
        },
        mocha_istanbul: {
            coverage: {
                src: 'tests', // the folder, not the files,
                options: {
                    mask: '*.js',
                    reportFormats: ['cobertura', 'html', 'lcovonly']
                }
            }
        }
    });

    // Load Tasks

    grunt.loadNpmTasks('grunt-mocha-istanbul');

    grunt.registerTask('test', ['mocha_istanbul:coverage']);
    grunt.registerTask('default', ['mocha_istanbul:coverage']);

};

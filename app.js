'use strict';

var app, express, path, favicon, bodyParser, index, show;

express = require('express');
path = require('path');
favicon = require('serve-favicon');
bodyParser = require('body-parser');
index = require('./routes/index');
show = require('./routes/show');
app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', index);
app.use('/index', index);
app.use('/show', show);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// catch 500 errors
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

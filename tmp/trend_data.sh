#!/usr/bin/env bash
PAGECOUNT=$1
for i in `seq 1 ${PAGECOUNT}`;
do
    curl --silent "https://www.google.com/trends/hottrends/atom/feed?pn=p$i" | grep 'ht:news_item_url' |cut -d'>' -f2 | cut -d'<' -f1
done

#REMOVE DUPES FROM FILE
#awk '!a[$0]++' bulk.list >bulk_url.list

